# README #

This file explains what this repository (repo) includes and how to engage with it.

### What is this repository for? ###

* All members of the North Star Project team are invited to contribute to this repo in order to define key data terms to be used across our business operations.
* For an introduction to version control tools like Bitbucket, please review the "What is version control" tutorial on [this Atlassian lesson page](https://confluence.atlassian.com/get-started-with-bitbucket/get-started-with-bitbucket-cloud-856845168.html).

### Contribution guidelines ###

* Make sure to log in to BitBucket with your eHealth Atlassian account (what you use with JIRA)
* Please copy (or "spoon") the repo folder directory (or "table cloth") in to your own workspace so that you can make changes on your own time.
* When ready to propose your changes back to the master version, submit a red-line type of revision (called a "pull request") to have your proposed changes reviewed.
* If accepted, the original file author will "merge" your changes.
* Here is an additional tutorial on [branches and pull requests](https://confluence.atlassian.com/get-started-with-bitbucket/work-on-branches-and-pull-requests-862720851.html). 

### Who do I talk to? ###

* This repo is managed by Ian Kalin

* This is a test change.